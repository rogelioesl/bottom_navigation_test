import 'package:bottom_navigation_test/pages/firts_page.dart';
import 'package:flutter/material.dart';

class PrincipalPage extends StatefulWidget {
  @override
  _PrincipalPageState createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> {

  int _selectedIndex = 0;
    static TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    static List<Widget> _widgetOptions = <Widget>[
    
    FirtsPage(),

    Text(
      'Index 1:',
      style: optionStyle,
    ),
    Text(
      'Index 2:',
      style: optionStyle,
    ),
    Text(
      'Index 3:',
      style: optionStyle,
    ),
    Text(
      'Index 4:',
      style: optionStyle,
    ),
  ];


    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      //_selectedIndex = 1;
      
      //_pages = [_pageOferta, _page2, _page3];
      //_currentPage = _pageOferta;
    }

    void _onItemTapped(int index) {
        setState(() {
          _selectedIndex = index;
          //_currentPage = _pages[index];
        });
    }
  
   @override
  Widget build(BuildContext context) {
    return _contenedor(context);
  }

  _contenedor(context){
      return Scaffold(  
      body: 
      
      _widgetOptions.elementAt(_selectedIndex),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.web),
          onPressed: (){},
        ),
        
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavigationBar(        
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),           
            title: Text('Home'),          
          ),
          BottomNavigationBarItem(
            
            icon: Icon(Icons.business),
            title: Text('Two', style: TextStyle(fontSize: 15, color: Colors.white),),
          ),
          BottomNavigationBarItem(
            backgroundColor: Colors.lightBlue,
            icon: Icon(null),
            title: Text('', style: TextStyle(fontSize: 15, color: Colors.white),),
            
          ),
          BottomNavigationBarItem(
            
            icon: Icon(Icons.business),
            activeIcon: Icon(Icons.local_cafe),
            title: Text('three', style: TextStyle(fontSize: 15, color: Colors.white),),
          ),
          BottomNavigationBarItem(
            
            icon: Icon(Icons.list),
            title: Text('four', style: TextStyle(fontSize: 15, color: Colors.white),),
          ),
        ],      
        showUnselectedLabels: true,
        elevation: 0.0,
        iconSize: 20.0,
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.blueGrey,
        onTap: _onItemTapped,
      ),
    );
  }
}