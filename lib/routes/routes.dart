
import 'package:bottom_navigation_test/pages/firts_page.dart';
import 'package:bottom_navigation_test/pages/principal_page.dart';
import 'package:bottom_navigation_test/pages/second_page.dart';
import 'package:bottom_navigation_test/pages/third_page.dart';
import 'package:flutter/material.dart';


Map<String, WidgetBuilder> getApplicationRoutes(){
  return <String, WidgetBuilder> {
        "/"    : (BuildContext context) => PrincipalPage(),
        "/second" : (BuildContext context) => SecondPage(),
        "/third" : (BuildContext context) => ThirdPage(),
        "/firts" : (BuildContext context) => FirtsPage(),
      };
}